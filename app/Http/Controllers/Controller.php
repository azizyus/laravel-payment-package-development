<?php

namespace App\Http\Controllers;

use Azizyus\Payment\Controllers\PaymentControllerComponent;
use Azizyus\Payment\Controllers\PaymentFrontControllerComponent;
use Azizyus\Payment\Forms\PaymentForm;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Redirect;
use Kris\LaravelFormBuilder\FormBuilder;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    protected $paymentControllerComponent;

    public function __construct()
    {
        $this->paymentControllerComponent = new PaymentControllerComponent();
    }

    public function index()
    {

    }

    public function create(FormBuilder $formBuilder)
    {
        return view('edit')->with($this->paymentControllerComponent->create());
    }

    public function store(Request $request)
    {
        $this->paymentControllerComponent->store($request);
        return Redirect::route('paymentAdmin.index');
    }

    public function edit(Request $request,$id)
    {
        return view('edit')->with($this->paymentControllerComponent->edit($id));
    }

    public function update(Request $request,$id)
    {
        $this->paymentControllerComponent->update($request,$id);
        return Redirect::route('paymentAdmin.index');
    }


}
