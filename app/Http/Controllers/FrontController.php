<?php

namespace App\Http\Controllers;

use Azizyus\Payment\Controllers\PaymentControllerComponent;
use Azizyus\Payment\Controllers\PaymentFrontControllerComponent;
use Azizyus\Payment\Forms\PaymentForm;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Redirect;
use Kris\LaravelFormBuilder\FormBuilder;

class FrontController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    protected $frontComponent;

    public function __construct()
    {
        $this->frontComponent = new PaymentFrontControllerComponent();
    }

    public function index($id)
    {
        return view('frontIndex')->with($this->frontComponent->index($id));
    }




}
