<?php


namespace App\Language;


use Azizyus\LaravelLanguageHelper\App\Models\LanguageWithoutDeleted;
use Azizyus\Payment\ExternalStructureDefinitions\ILanguage;

class Language extends LanguageWithoutDeleted implements ILanguage
{

}
