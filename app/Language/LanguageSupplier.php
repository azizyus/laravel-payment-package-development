<?php


namespace App\Language;


use Azizyus\LaravelLanguageHelper\App\Repositories\Eloquent\LanguageRepository;
use Azizyus\Payment\ExternalStructureDefinitions\ILanguageSupply;

class LanguageSupplier extends LanguageRepository implements ILanguageSupply
{
    public function all()
    {
        return $this->getAll();
    }
}
