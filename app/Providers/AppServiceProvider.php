<?php

namespace App\Providers;

use App\Currency\Currency;
use App\Currency\CurrencySupplier;
use App\Language\Language;
use App\Language\LanguageSupplier;
use Illuminate\Support\ServiceProvider;
use Azizyus\Payment\ExternalStructureDefinitions\ICurrencySupply;
use Azizyus\Payment\ExternalStructureDefinitions\ILanguage;
use Azizyus\Payment\ExternalStructureDefinitions\ICurrency;
use Azizyus\Payment\ExternalStructureDefinitions\ILanguageSupply;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind(ILanguage::class,Language::class);
        app()->bind(ICurrency::class,Currency::class);
        app()->bind(ICurrencySupply::class,function($app){
            return new CurrencySupplier();
        });
        app()->bind(ILanguageSupply::class,function($app){
            return new LanguageSupplier();
        });

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
