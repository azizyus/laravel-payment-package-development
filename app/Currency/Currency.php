<?php


namespace App\Currency;


use Azizyus\Payment\ExternalStructureDefinitions\ICurrency;

class Currency extends \CurrencyManager\Models\Currency implements ICurrency
{
    public function getTripleCode()
    {
        return $this->codeAlpha;
    }

    public function getSymbol()
    {
        return $this->symbol;
    }

    public function getCodeNumeric()
    {
        return $this->codeNumeric;
    }


}
