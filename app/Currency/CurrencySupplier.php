<?php


namespace App\Currency;


use Azizyus\Payment\ExternalStructureDefinitions\ICurrencySupply;
use CurrencyManager\BaseQueryImplementations\CurrencyBaseQuery;
use CurrencyManager\Models\Currency;
use CurrencyManager\Repositories\CurrencyRepository;

class CurrencySupplier extends CurrencyRepository implements ICurrencySupply
{
    public function __construct()
    {
        $this->setBaseQueryImplementation(new CurrencyBaseQuery());
    }

    public function first($id)
    {
        return $this->baseQuery()->where('id',$id)->first();
    }
}
