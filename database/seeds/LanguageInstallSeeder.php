<?php

use Illuminate\Database\Seeder;

class LanguageInstallSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $language = new \Azizyus\LaravelLanguageHelper\App\Models\Language();

        $language->title = "English";
        $language->shortTitle = "EN";
        $language->isActive = true;

        $language->save();
    }
}
